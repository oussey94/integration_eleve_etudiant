package com.mbodji.integration;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mbodji.integration.entities.Eleve;
import com.mbodji.integration.entities.Etudiant;
import com.mbodji.integration.services.EleveService;
import com.mbodji.integration.services.EtudiantService;

@SpringBootApplication
public class Integration2021Application implements CommandLineRunner{
	@Autowired
	private EtudiantService etudiantService;
	
	@Autowired
	private EleveService eleveService;

	public static void main(String[] args) {
		SpringApplication.run(Integration2021Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		etudiantService.addNewEtudiant(new Etudiant(null, "Lo","Cheikh Ahmeth Tidiane","IMG-20210201-WA0041 - Cheikh Lo.jpg","SupDéCo,UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhate","Aissatou", "FE90B6C3-70E8-4155-9C7A-67F802FDBE65 - aissatou diakhate.jpeg","ESP","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ndiaye","Albert Theophil Junior","20210612_122718 - Albert Ndiaye.jpg","Ecole_Douane","L","CEERSO", new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Dramé","Moustapha","IMG-20210516-WA0004 - Moustapha Dramé.jpg","UCAD","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Fall","NDEYE MANE","B895A72D-89B1-4C7B-87D9-B7D14B5D6876 - Ndèye Mané FALL.jpeg","ESP","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Camara","Mame Bassine","IMG-20210504-WA0037 - Mame Bassine Camara.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Dieng","Mariètou ","Screenshot_20210617-093238-1 - Mariètou Dieng.jpg","UCAD","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sarr","Demba","FB_IMG_1612746611196 - Demba sarr.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sow","Ndeye Fatou","IMG-20201116-WA0013 - ndeye fatou sow.jpg","UCAD","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diop","Khalifa","image - Khalifa Diop.jpeg","HEMI_UCAD2","L","AMEERN",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Ndiaye","MALICK THIAW ","IMG-20210602-WA0000 - Malick thiaw Ndiaye.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diome","Mouhamadou Bamba","17996DED-FA98-4978-98A5-C8CF36A2BFD7 - mouhamadou bamba diome.jpeg","ESP","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Donde","Dieynaba","PhotoGrid_1603480183505 - Dieynaba Dondé.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Mbengue","Marcelle","8E027041-0B8B-402A-BB86-B52F4E24BFCC - Marcelle Mbengue.jpeg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Faye","Abdourahmane","AAC80E2E-F9AA-4437-B57F-32CACCCC146B - Abdourahmane FAYE.jpeg","ESP","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diop","Ndeye Mbarou ","20210129_154018 - Mame Mbarou DIOP.jpg","ISEG_UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Thiaw ","Abdoulaye ","IMG-20191007-WA0037 - Abdoulaye Thiaw.jpg","UCAD","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Thiam","Bassirou","IMG_20210529_143635_415 - Mouhamed Bassirou Thiam.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Boye","Mounirou","IMG-20210119-WA0011 - Mounirou Boye.jpg","Ipg/isti","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhaté","Astou","Snapchat-578906119 - Astou Diakhaté.jpg","HEMI","L","CEERSO",new ArrayList<>()));
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Boye","Serigne Modou","7456E29E-F31D-4CE7-A0CC-F9022724EBAD - Serigne Boye.jpeg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Mbengue","Fatim","tim.jpeg","IMAN","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Babacar","IMG_20201125_171836_498 - Babacar Sadji.jpg","Keur_Yakaar","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Dieng","Faly","2DAE2902-0556-4657-B596-C432A61F007D - Faly Dieng.jpeg","Ensup","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diom","Mouhamadou ","IMG_20210316_081952_216 - Diom Mouhamadou.jpg","UCAO","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Faye","Maguette","B6979D81-DB35-4B87-834D-597F086D2663 - Maguette Faye.png","Ensup","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Cissé","Modou","Screenshot_20210518-160908 - Modou Cisse.png","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Dieng","Salimata","IMG-20210607-WA0009 - Salimata Dieng.jpg","Inseps","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Baye Daraw","IMG_20200421_111340_578 - Baye Daraw Sadji.jpg","ISEP_Thies","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Boye","Goumba dieng","BBA0A160-7739-4C7D-B089-3B9DA91ACC2E - Goumba dieng Boye.jpeg","UCAD","L","CEERSO",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Papa","Screenshot_20200401-152149 - PapeLine Nitory.png","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhaté","Fatou","IMG-20210603-WA0026 - Fatou Diakhate.jpg","HEMI","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Mbengue ","Baidy","231DF6C1-2956-42B4-9101-D381E37F8082 - Baidy Mbengue.jpeg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhaté","Rokhaya","IMG-20210609-WA0010 - kya diakhaté.jpg","HEMI","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sow","Chérif Hafisse","38DDB118-7CE9-4F61-B964-3B6E77926E75 - cherif sow.jpeg","HEMI","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ndoye","Assane","IMG_20210513_153145_4 - Assane ndoye.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sow","mame Ndoye ","Snapchat-732565067 - mame Aïcha sow.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ciss","Maodo Malick","FaceApp_1623355823891 - Maodo Malick Ciss.jpg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Lo","Mamadou","20210322_170810 - dwight mcstevens.jpg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Yacine","Screenshot_20201008-224700 - yacine sadji.png","UCAD","L","CEERSO",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Mbodji","Ousseynou","m.jpeg","UCAD","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Yade","Alioune badara sakho","IMG_20210329_180659_013 - Alioune Badara S Yade.jpg","UCAD","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Salimata","IMG-20210403-WA0020 - Sadji Salimata.jpg","ESP","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Thiam","Françoise Coumba","IMG_20210406_010013_079 - Thiam Françoise.jpg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sarr","Fatou","20210513_213754 - Fatou Sarr.jpg","Ipd Thomas sankara ","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Seck","Aminata","5C9DE3A3-C9D2-40F4-A6F4-702BB4E206A0 - Aminata SECK.jpeg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Gueye","Abdoulaye","IMG-20201013-WA0008 - abdoulaye gueye.jpg","UCAD","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakahté","Baye Diallo","20210418_190051 - Baye Diallo Diakhaté.jpg","UCAO","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Faye","Mareme","IMG-20210306-WA0017 - Maréme Faye.jpg","Batisup","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diouf","Abdoulaye","Screenshot_20210605-231702 - Abdoulaye Diouf.png","CFPC DELAFOSSE","L","CEERSO",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhaté","Khady","Screenshot_20210120-082213 - Khady Diakhaté.png","G15","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sarr","Adama","IMG_20210310_100845_120 - adama sarr.jpg","UCAD","S","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Adji Bineta","IMG-20210513-WA0013 - Adji Bineta Sadji.jpg","Ipg/Isti","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Faye","Awa","IMG_20210507_091957_001 - Awa Faye.jpg","Sup santé Dakar Mermoz","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diouf","Khady","IMG_20201029_124555_222 - Khady Diouf.jpg","IPG-ISTI","S","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ciss","Ousmane","IMG_4068 2 - ousmane noreyni ciss.JPG","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diakhaté","Marième","Screenshot_20210131-220927 - Marieme Diakhate.png","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Mbengue","Mamadou","BF7D7337-AAC9-4D92-8A45-66EEDEB81483 - Mariane Sock.jpeg","Ensup","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sembene","Abdoulaye","IMG-20210609-WA0014 - Abdoulaye Sembene.jpeg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Dieng","Birame","DC9885C8-6741-49E2-B4EF-274F17FDC059 - Birame DIENG.jpeg","UCAD","L","AMEERN",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

		etudiantService.addNewEtudiant(new Etudiant(null, "Sagne","Ouraye","7B49C4C0-EB3F-4DFA-931E-0EB248BBE753 - Ouraye Sagne.jpeg","HECM","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diané","Oumy","814CCB21-307F-45CE-83A6-EDF1245359D2 - Oumy Diane.png","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ndour","MARTHE YVONNE DIBOR","IMG-20210618-WA0013 - Yacine Dial Cissé.jpg","UCAD","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ndour","Mansour","mass.jpeg","IPG/ISTI","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Sadji","Abdou karim","pabi.jpeg","UCAD","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Ndiaye","Mariama","IMG-20210318-WA0057 - Mariama NDIAYE.jpg","Université du sahel","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Camara","Coumba","Snapchat-235277775 - Coumba Kamara.jpg","ESTEL","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Tall","Seynabou","nabou.jpeg","HEMI","L","CEERSO",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Faye","Elhadji","CB00E43B-4E3B-49BF-986A-33AE6D7BC949 - elhadji faye.jpeg","Ensup","L","AMEERN",new ArrayList<>()));
		etudiantService.addNewEtudiant(new Etudiant(null, "Diome","Moussa","08892799-27DE-4AB3-9D27-19256F263D3C - Moussa Diome.png","UCAD","L","AMEERN",new ArrayList<>()));

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	
		
		
		eleveService.addNewEleve(new Eleve(null,"Lô"," Sanon","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Mbengue","Aïssatou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Diagne","Fatou Sakho","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Diakhaté","Aïssatou Moussa","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Dione","Fatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Dièn","Soukeyna","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Diakhaté","Awa","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Bakhoum","Rosalie","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Ba","Aminata","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Ngom","Elisabeth","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Thioub","Adja Khady","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Ndione","Babacar","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Dia","Fall","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Diallo","Issa","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Sow","Maïmouna","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Sène","Ndèye Marie","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Ndiaye","Mouhamed Rassoul","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Fall","Absa","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOP","Diarra","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Yandé","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GUEYE","Nogoye","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Dione","Seynabou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIENG","Khady","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOUF","Rokhayatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAKHATE","Diarra","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIENNE","Maguette","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SOW","Aïssatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Diallo","Mariama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"CISS","Dieynaba","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Soda","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"FALL","Babacar assane","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SENE","Hélène","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GUEYE","Seynabou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIENG","Abdoulaye","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"DIAKHATE","Arsou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"MBENGUE","Codou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"THIAW","Ouraye","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Ndèye Dimingua","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Amy","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"THIAW","Binetou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"BADIANE","Oulimatou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"BADIANE","El Hadji Chérif","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBAYE","Coumba","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NGOM","Rokhaya","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GAYE","Mame Médina","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GAYE","Khadidiatou Mbenda","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Sofiétou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Ndèye Absa","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Ndèye Dibor","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Ndèye","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"DIOME","Maguette","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBODJI","Awa","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GUEYE","Yacine","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GUEYE","Yaba","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"GBEDEKO","Erica Consteretina Elisa","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Mariama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDOYE","Souba","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAKHATE","Aïssatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"PAMY","Elisabeth","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAKHATE","Diatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Rokhaya","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"YADE","Fatoumata","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOP","Fatim","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NIANG","Ndiapaly","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Fatim","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"GNINGUE","Maïmouna","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Sofiètou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Mouhamed","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Mame Thioro","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"LO","Khalidou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"Diallo","Fatou","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Ngoné","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Djibril","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Maïmouna","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Doudou Georges","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"DIAGNE","Mariama Seyni","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAGNE","Rama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SANE","Aïssatou Combé","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"SOW","Hawa","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DABO","Arame","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDOUR","Emmanuel","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SENE","Fatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDIONE","Penda","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SADJI","Dior","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"SECK","Fama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"BA","Fatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"BA","Mariama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SENGHOR","Rose Amy","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Ndiaya","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAGNE","Diarry","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"TOURE","Mouhamed","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"POUYE","Mame Anta","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIONE","Omar","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"BOP","Diegane","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIENG","Mouhamed S","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Dié Aminata M","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SEYE","Fatou Mané","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GUEYE","Amy","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"SALL","Amy","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"Diallo","Yankhouba","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Binta","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NIANG","Cheikh","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"YADE","Mame Fatim","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SENE","Sokhna.M.M","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Aminata","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SOW","Sidi Dagana","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"BA","Mamadou Baïlo","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Arame","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"LO","Latyr","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Ndèye","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"NGUIRANE","Ndiatté","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GAKOU","Saliou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SAGNE","Ndèye Fatou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOUF","Adama","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"SAMB","Amadou","unknown.png","Lycée Serigne Mamadou Léna Diop","L",null));
		eleveService.addNewEleve(new Eleve(null,"GBEDEKO","André Rosalie R","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDOYE","Mbasse","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"BAH","Mariama","unknown.png","Lycée Serigne Mamadou Léna Diop","S",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Ramata","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"HAÏDOU","Khar","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"BOYE","Fanta Amélie","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"MANGA","Marième","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBENGUE","Sonya","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Rokhaya","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"KONATE","Fatou Bintou","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIAKHATE","Seynabou","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBAYE","Aminata","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"BOYE","Mariama","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"DIEDHIOU","Ndèye Marième","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOUF","Aïssatou","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Binta","unknown.png","Collège Tafsir Yaté Ciss","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Khoudia","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Dieynaba","unknown.png","Collège Tafsir Yaté Ciss","S",null));
		eleveService.addNewEleve(new Eleve(null,"DJIGUEUL","Diodio","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DJITE","Ibrahima","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"BA","Diary","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SECK","Boubacar","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"BA","Aïssata","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDIAYE","Aïssatou Paye","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Omar","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"YOUM","Fatou","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SENE","Ndèye Yacine","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SANTHIE","Mame Aminata","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBENGUE","Ibrahima","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Fatou","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"MBENGUE","Pape Omar","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"NDOYE","Ndèye","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"NGOM","Papa","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Mariètou","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Mami Aïssatou","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SALL","Moustapha","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIENG","Anna","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"SARR","Adama","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DEME","Abdoulaye","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIALLO","Isseu","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIOUF","Marie","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DIALLO","Mamadou Lamine","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"FAYE","Ndèye penda","unknown.png","Ecole Demba Thiague Diouf","L",null));
		eleveService.addNewEleve(new Eleve(null,"DONDE","Pape Cheikhou","unknown.png","Ecole Demba Thiague Diouf","L",null));




	}

}
