package com.mbodji.integration.restControllers;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mbodji.integration.entities.Eleve;
import com.mbodji.integration.entities.Etudiant;
import com.mbodji.integration.repository.EtudiantRepository;
import com.mbodji.integration.services.EleveService;
import com.mbodji.integration.services.EtudiantService;

@RestController
@RequestMapping("/api1")
@CrossOrigin("*")
public class EleveRestController {
	@Autowired
	private EleveService eleveService;
	
	@Autowired
	private EtudiantService etudiantService;
	
	@GetMapping
	List<Eleve> getAll(){
		return eleveService.getAllEleve();
	}
	
	@GetMapping("/{id}")
	public Eleve getByid(@PathVariable("id")Long id) {
		return eleveService.getParEleveId(id);
	}
	
	@PostMapping
	public Eleve addEleve(@RequestBody Eleve eleve) {
		return eleveService.addNewEleve(eleve);
	}
	
	@PutMapping("/{id}")
	public Eleve updateElev( @PathVariable("id") Long id, @RequestBody Eleve eleve) {
		Eleve e=eleveService.getParEleveId(id);
		e.setNomEleve(eleve.getNomEleve());
		e.setPrenomEleve(eleve.getPrenomEleve());
		e.setPhotoEleve(eleve.getPhotoEleve());
		e.setEtablissementEleve(eleve.getEtablissementEleve());
		e.setDomaineEleve(eleve.getDomaineEleve());
		e.setEtudiant(eleve.getEtudiant());
		
		return eleveService.updateEleve(eleve);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteEl(@PathVariable("id")Long id) {
		eleveService.deleteEleveParId(id);;
	}
	@GetMapping(path = "/parrainage")
	public List<Eleve> getAllParrains(){
		List<Eleve> eleves= eleveService.getAllEleve();
		List<Etudiant> etudiants= new ArrayList<>();
		
		for ( Eleve eleve: eleves) {
			Random rand= new Random();
			etudiants= etudiantService.getEtudiantParDomaine(eleve.getDomaineEleve());
			
			Etudiant etudiant= etudiants.get(rand.nextInt(etudiants.size()));

			eleve.setEtudiant(etudiant);
		}
		
		return eleves;
	}

	@GetMapping(path="/photoEleve/{id}", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getPhoto(@PathVariable("id") Long id) throws Exception {
		Eleve el= eleveService.getParEleveId(id);
		return Files.readAllBytes(Paths.get(System.getProperty("user.home")+"/Documents/integration/etudiants/"+el.getPhotoEleve()));
	}

}
