package com.mbodji.integration.restControllers;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mbodji.integration.entities.Etudiant;
import com.mbodji.integration.services.EtudiantService;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api2")
@CrossOrigin("*")
public class EtudiantRestController {
	@Autowired
	private EtudiantService etudiantService;
	
	@GetMapping
	public List<Etudiant> getAll() {
		return etudiantService.getAllEtudiant();
	}
	
	@GetMapping("/{id}")
	public Etudiant getByid(@PathVariable("id")Long id) {
		return etudiantService.getEtudiantParId(id);
	}
	
	@PostMapping
	public Etudiant addEtudiant(@RequestBody Etudiant etudiant) {
		return etudiantService.addNewEtudiant(etudiant);
	}
	
	@PutMapping("/{id}")
	public Etudiant updateEtu( @PathVariable("id") Long id, @RequestBody Etudiant etudiant) {
		Etudiant e=etudiantService.getEtudiantParId(id);
		e.setNomEtudiant(etudiant.getNomEtudiant());
		e.setPrenomEtudiant(etudiant.getPrenomEtudiant());
		e.setPhotoEtudiant(etudiant.getPhotoEtudiant());
		e.setEtablissementEtudiant(etudiant.getEtablissementEtudiant());
		e.setDomaineEtudiant(etudiant.getDomaineEtudiant());
		e.setAmical(etudiant.getAmical());
		e.setEleves(etudiant.getEleves());
		
		return etudiantService.updateEtudiant(etudiant);
		
	}
	
	@DeleteMapping("/{id}")
	public void deleteEtu(@PathVariable("id")Long id) {
		etudiantService.deleteEtudiantParId(id);
	}

	@GetMapping(path="/photoEtudiant/{id}", produces = MediaType.IMAGE_PNG_VALUE)
	public byte[] getPhoto(@PathVariable("id") Long id) throws Exception {
		Etudiant et= etudiantService.getEtudiantParId(id);
		return Files.readAllBytes(Paths.get(System.getProperty("user.home")+"/Documents/integration/etudiants/"+et.getPhotoEtudiant()));
	}

	@PostMapping(path="/uploadPhoto/{id}")
	public void uploadPhoto(MultipartFile file, @PathVariable("id") Long id) throws Exception {
		Etudiant e=etudiantService.getEtudiantParId(id);
		e.setPhotoEtudiant(file.getOriginalFilename());
		Files.write(Paths.get(System.getProperty("user.home")+"/Documents/integration/etudiants/"+e.getPhotoEtudiant()),file.getBytes());

		etudiantService.addNewEtudiant(e);
	}

}
