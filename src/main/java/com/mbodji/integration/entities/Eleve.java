package com.mbodji.integration.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Eleve {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idEleve;
	private String nomEleve;
	private String prenomEleve;
	private String photoEleve;
	private String etablissementEleve;
	private String domaineEleve;
	//@JsonIgnore
	@ManyToOne
	private Etudiant etudiant;
	

}
