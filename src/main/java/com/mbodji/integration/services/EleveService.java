package com.mbodji.integration.services;

import java.util.List;

import com.mbodji.integration.entities.Eleve;

public interface EleveService {
	Eleve addNewEleve(Eleve eleve);
	Eleve updateEleve(Eleve eleve);
	List<Eleve> getAllEleve();
	Eleve getParEleveId(Long id);
	void deleteEleveParId(Long id);

}
