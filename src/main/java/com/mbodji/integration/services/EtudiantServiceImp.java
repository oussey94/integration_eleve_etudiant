package com.mbodji.integration.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.mbodji.integration.entities.Etudiant;
import com.mbodji.integration.repository.EtudiantRepository;

@Service
public class EtudiantServiceImp implements EtudiantService{
	private EtudiantRepository etudiantRepository;
	

	public EtudiantServiceImp(EtudiantRepository etudiantRepository) {
		super();
		this.etudiantRepository = etudiantRepository;
	}

	@Override
	public Etudiant addNewEtudiant(Etudiant etudiant) {
		// TODO Auto-generated method stub
		return etudiantRepository.save(etudiant);
	}

	@Override
	public Etudiant updateEtudiant(Etudiant etudiant) {
		// TODO Auto-generated method stub
		return etudiantRepository.save(etudiant);
	}

	@Override
	public List<Etudiant> getAllEtudiant() {
		// TODO Auto-generated method stub
		return etudiantRepository.findAll();
	}

	@Override
	public Etudiant getEtudiantParId(Long id) {
		// TODO Auto-generated method stub
		return etudiantRepository.findById(id).get();
	}

	@Override
	public void deleteEtudiantParId(Long id) {
		// TODO Auto-generated method stub
		etudiantRepository.deleteById(id);
		
	}

	@Override
	public List<Etudiant> getEtudiantParDomaine(String domaine) {
		// TODO Auto-generated method stub
		return etudiantRepository.findEtudiantsByDomaineEtudiantIs(domaine);
	}


}
