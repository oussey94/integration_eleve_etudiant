package com.mbodji.integration.services;

import java.util.List;

import org.springframework.stereotype.Component;

import com.mbodji.integration.entities.Etudiant;
@Component
public interface EtudiantService {
	Etudiant addNewEtudiant(Etudiant etudiant);
	Etudiant updateEtudiant(Etudiant etudiant);
	List<Etudiant> getAllEtudiant();
	Etudiant getEtudiantParId(Long id);
	void deleteEtudiantParId(Long id);
	List<Etudiant> getEtudiantParDomaine(String domaine);

}
