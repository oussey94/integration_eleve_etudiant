
package com.mbodji.integration.services;

import java.util.List;

import org.springframework.stereotype.Service;


import com.mbodji.integration.entities.Eleve;
import com.mbodji.integration.repository.EleveRepository;
@Service
public class EleveServiceImp implements EleveService{
	private EleveRepository eleveRepository;
	

	public EleveServiceImp(EleveRepository eleveRepository) {
		super();
		this.eleveRepository = eleveRepository;
	}

	@Override
	public Eleve addNewEleve(Eleve eleve) {
		// TODO Auto-generated method stub
		return eleveRepository.save(eleve);
	}

	@Override
	public Eleve updateEleve(Eleve eleve) {
		// TODO Auto-generated method stub
		return eleveRepository.save(eleve);
	}

	@Override
	public List<Eleve> getAllEleve() {
		// TODO Auto-generated method stub
		return eleveRepository.findAll();
	}

	@Override
	public void deleteEleveParId(Long id) {
		// TODO Auto-generated method stub
		eleveRepository.deleteById(id);
		
	}

	@Override
	public Eleve getParEleveId(Long id) {
		// TODO Auto-generated method stub
		return eleveRepository.findById(id).get();
	}

}
