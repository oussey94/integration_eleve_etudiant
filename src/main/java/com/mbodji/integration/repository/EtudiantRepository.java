package com.mbodji.integration.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbodji.integration.entities.Etudiant;
@Repository
public interface EtudiantRepository extends JpaRepository<Etudiant, Long>{
	//List<Etudiant> findEtudiantsByDomaineEtudiant(String domaineEtudiant);
	List<Etudiant> findEtudiantsByDomaineEtudiantIs(String domain);

}
