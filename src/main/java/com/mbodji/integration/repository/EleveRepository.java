package com.mbodji.integration.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.mbodji.integration.entities.Eleve;
@Repository
public interface EleveRepository extends JpaRepository<Eleve, Long> {

}
